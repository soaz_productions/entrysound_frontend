export default (data, callback) => {
  let reader = new FileReader();
  reader.readAsDataURL(data);
  reader.onload = () => {
    callback(reader.result);
  };
}