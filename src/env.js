const apiRoot = () => {
  if (window.env && window.env.API_ROOT) {
    return window.env.API_ROOT;
  } else {
    return 'https://api.entrysound.long-bui.de';
  }
};

const clientID = () => {
  if (window.env && window.env.CLIENT_ID) {
    return window.env.CLIENT_ID;
  } else {
    return '569850964383891471';
  }
};

const redirect = () => {
  return window.origin + '/redirect';
};

const oauthURI = () => {
  return `https://discordapp.com/api/oauth2/authorize?client_id=${clientID()}&redirect_uri=${redirect()}&response_type=token&scope=identify`;
};

export const API_ROOT = apiRoot();
export const CLIENT_ID = clientID();
export const REDIRECT = redirect();
export const OAUTH_URI = oauthURI();
